/**
 * Created by mas_bk on 5/7/14.
 */
var express = require('express'),
	game = express.Router();

game.get('/', function (req, res) {
	res.render('game');
});

module.exports = game;


'use strict';

define([],function(){
	var AssetsLoader = function(){

		this.load = function(){
			app.game.load.image('mylogo', 'assets/html5-logo.png');
			app.game.load.start();
		};

		this.onLoadStart = function(){
			if(app.objects().preloader){
				app.objects().preloader.setText("loading 0%");
			}
		};

		this.onLoadComplete = function(){
			app.objects().preloader.visible = false;
			app.loadAssetsComplete();
		};

		this.onFileComplete = function(progress, cacheKey, success, totalLoaded, totalFiles){
			app.objects().preloader.setText("loading "+progress+'%');
		};

		app.game.load.onLoadStart.add(this.onLoadStart, this);
		app.game.load.onLoadComplete.add(this.onLoadComplete, this);
		app.game.load.onFileComplete.add(this.onFileComplete, this);

	};
	return AssetsLoader;
})

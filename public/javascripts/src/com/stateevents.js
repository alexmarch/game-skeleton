'use strict';

define(['Phaser', 'src/conf/gameconf', 'src/com/assetsloader'], function (Phaser, conf, AssetsLoader) {
	var StateEvents = function (app) {
		var _app = app;
		this.preload = function () {
			//@todo preload
		};
		this.create = function () {
			_app.game.stage.backgroundColor = conf.backgroundColor;
			_app.objects().preloader = _app.game.add.text(0, 0, 'loading 0%', {fill: '#000' });
			_app.objects().preloader.position.x = _app.game.width / 2 - _app.objects().preloader._width / 2;
			_app.objects().preloader.position.y = _app.game.height / 2 - _app.objects().preloader._height;
			this.start();
		};
		this.update = function () {
			if(_app && typeof _app.update === "function" ){
				_app.update(_app)
			}
		};
		this.render = function () {
			//@todo render
		};
		this.start = function () {
			var loader = new AssetsLoader();
			_app.objects().loader = loader;
			loader.load();
		}

	};
	return StateEvents;
});

'use strict';

define(['jquery', 'Phaser', 'src/com/stateevents', 'src/conf/gameconf'], function ($, Phaser, StateEvents, conf) {
	//Create base game object
	var App = App || {};
	$(document).ready(function () {
		$('#game').width(conf.width).height(conf.height);
		App = function () {
			var _objects = {};
			var events = new StateEvents(this);
			this.game = new Phaser.Game(conf.width, conf.height, Phaser.AUTO, 'game', events);
			this.objects = function () {
				return _objects
			};
			this.loadAssetsComplete = function () {
				var spLogo = this.game.add.sprite(0, 0, 'mylogo');
				var sp2Logo = this.game.add.sprite(0, 0, 'mylogo');
				var text = this.game.add.text(32,32,"This is game skeleton",{fill:'#000'});
				text.position.x = this.game.width / 2 - text.width / 2;
				spLogo.position.x = this.game.width / 2;
				spLogo.position.y = this.game.height / 2;
				spLogo.anchor.setTo(0.5,0.5);spLogo.position.x = this.game.width / 2;

				sp2Logo.position.x = this.game.width / 2;
				sp2Logo.position.y = this.game.height / 2;
				sp2Logo.anchor.setTo(0.5,0.5);
				this.objects().splogo = spLogo;
				this.objects().sp2logo = sp2Logo;
			};
			this.update = function(app){
				if(app.objects().splogo	){
					app.objects().splogo.angle ++;
					app.objects().sp2logo.angle --;

				}
			};
		};
		window.app = new App();
	})
});

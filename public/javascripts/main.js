/**
 * Created by mas_bk on 5/7/14.
 */

requirejs.config({
	baseUrl: '/javascripts',
	paths: {
		'jquery': 'libs/jquery.min',
		'Phaser': 'frameworks/phaser.min',
		'App': 'src/app'
	},
	shim:{
		'app': {
			deps: ['jquery', 'phaser'],
			exports: 'App'
		}
	}
});

require(['jquery','Phaser','App'],function(App){
	console.log("Javascript loaded");
});


